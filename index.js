const path = require("path")
const fs = require("fs")

const readXlsxFile = require('read-excel-file/node');

const emailSender = "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"
const subject = "Job application – Senior Frontend Developer - Federico Fernandez"
const emailRemitente = "me@federico08.xyz"
const nameRemitente = "Federico Fernández"
const templateId = "d-b9e90d343be04d17b527e459100ddd4e"
const fileName = "FedericoFernandezResume.pdf";
const xlsxFileName = `EmailsDB.xlsx`;
const sheetXlsxName = `Angel List`;

const pathXlsx = `${path.resolve()}/${xlsxFileName}`;

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(emailSender);

const toSend = []

const filePath = `${path.resolve()}/${fileName}`
const bitmap = fs.readFileSync(filePath);
const ResumeBase64 =  new Buffer(bitmap).toString('base64');

function PushMsgs(){
  const persons = [];
  readXlsxFile(pathXlsx, { sheet: sheetXlsxName }).then((data) => {
    const sliced = data.slice(12, data.length)
    sliced.forEach(function (dta){
      if(dta[3] !== null){
        if(dta[3].length > 0){
          const objPersons = dta[3].split("$");
          objPersons.forEach(function(dtaPerson){
            const toPush = dtaPerson.split(",")
            persons.push({
              name: toPush[0],
              email: toPush[1],
              company: dta[0]
            })
          })
        }
      }
    })
  })
  persons.forEach(function(mail){
    const msg= {
      to: mail.email,
      from: {
        email: emailRemitente,
        name: nameRemitente
      },
      templateId: templateId,
      subject: subject,
      attachments: [
        {
          content: ResumeBase64,
          filename: fileName
        }
      ],
      dynamic_template_data: {
        subject: subject,
        name: mail.name,
        company: mail.company,
      }
    };
    toSend.push(msg)
  })
}

function Send(){
  sgMail
    .send(toSend)
    .then(() => {console.log("Success")}, error => {
      console.error(error);

      if (error.response) {
        console.error(error.response.body)
      }
    });
}
PushMsgs()
// Send()
// console.log(ResumeBase64)


